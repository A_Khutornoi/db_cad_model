from django.shortcuts import render,get_object_or_404
from .models import Main
import datetime
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


from performances.models import Performance

def index(request):
	main_list = Main.objects.all()

	now = datetime.datetime.now()
	perf = Performance.objects.filter(time__gt = now)

	paginator = Paginator(main_list, 5)
	page = request.GET.get('page')
	try:
		contacts = paginator.page(page)
	except PageNotAnInteger:
	# If page is not an integer, deliver first page.
		contacts = paginator.page(1)
	except EmptyPage:
	# If page is out of range (e.g. 9999), deliver last page of results.
		contacts = paginator.page(paginator.num_pages)

	context = {'main': contacts, 'now':now, 'perf':perf[:5]}
	return render(request, 'main_page.html', context)


def about(request, pk):
	news = get_object_or_404(Main, pk = pk)

	now = datetime.datetime.now()
	perf = Performance.objects.filter(time__gt = now)
	
	context = {'news':news,'now':now, 'perf':perf[:5]}
	return render(request,'about_news.html', context)
# Create your views here.


def default(request):

	now = datetime.datetime.now()
	perf = Performance.objects.filter(time__gt = now)
	
	context = {'now':now, 'perf':perf[:5]}
	return render(request,'index.html', context)
# Create your views here.