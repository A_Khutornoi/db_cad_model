# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_main_pub_date'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='main',
            options={'verbose_name': 'News', 'verbose_name_plural': 'News'},
        ),
        migrations.AddField(
            model_name='main',
            name='img',
            field=models.ImageField(blank=True, null=True, upload_to=''),
        ),
    ]
