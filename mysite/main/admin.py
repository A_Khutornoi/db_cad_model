from django.contrib import admin
from .models import Main

class NewsAdmin(admin.ModelAdmin):
	search_fields = ['title']
	list_display = ['title','pub_date']


admin.site.register(Main,NewsAdmin)
# Register your models here.
