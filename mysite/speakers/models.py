from django.db import models

class Speaker(models.Model):
	fio = models.CharField(max_length=70)
	description = models.TextField()
	photo = models.ImageField(upload_to="")
	
		
	def __unicode__(self):
		return self.fio

	def __str__(self):
		return self.fio

