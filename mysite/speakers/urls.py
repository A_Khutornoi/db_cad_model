from django.conf.urls import patterns, url

from speakers import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'^(?P<pk>\d+)$', views.about, name='about'),
)