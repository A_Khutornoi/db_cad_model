from django.contrib import admin
from .models import Speaker

class SpeakerAdmin(admin.ModelAdmin):
	search_fields = ['fio']
	
	
admin.site.register(Speaker,SpeakerAdmin )
# Register your models here.
