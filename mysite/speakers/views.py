from django.shortcuts import render,get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Speaker
import datetime
from performances.models import Performance

def index(request):
	speakers_list = Speaker.objects.all()

	now = datetime.datetime.now()
	perf = Performance.objects.filter(time__gt = now)
	
	paginator = Paginator(speakers_list, 6)
	page = request.GET.get('page')
	try:
		contacts = paginator.page(page)
	except PageNotAnInteger:
	# If page is not an integer, deliver first page.
		contacts = paginator.page(1)
	except EmptyPage:
	# If page is out of range (e.g. 9999), deliver last page of results.
		contacts = paginator.page(paginator.num_pages)
	context = {'speaker':contacts,'now':now, 'perf':perf[:5]}
	return render(request, 'speakers.html', context)

def about(request, pk):
	speaker = get_object_or_404(Speaker, pk = pk)

	now = datetime.datetime.now()
	perf = Performance.objects.filter(time__gt = now)
	
	context = {'speaker':speaker,'now':now, 'perf':perf[:5]}
	return render(request, 'about_speaker.html', context)