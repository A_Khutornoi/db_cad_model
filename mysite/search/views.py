from django.shortcuts import render
from django.http import HttpResponse
from speakers.models import Speaker
from organizers.models import Organizer
from sponsors.models import Sponsor
from performances.models import Performance
from main.models import Main
import datetime

# Create your views here.


def search(request):
    if 'q' in request.GET and request.GET['q']:
        q = request.GET['q']

        now = datetime.datetime.now()
        perf = Performance.objects.filter(time__gt = now)
        
        news = Main.objects.filter(title__icontains=q)            
        speakers = Speaker.objects.filter(fio__icontains=q)
        organizers = Organizer.objects.filter(fio__icontains=q) 
        sponsors = Sponsor.objects.filter(name__icontains=q) 
        performances = Performance.objects.filter(name__icontains=q)

        return render(request,'search.html',
            {'speakers': speakers,
            'organizers':organizers,
            'sponsors':sponsors,
            'performances':performances,
            'news':news,

            'now':now,
            'perf':perf[:5],

            'query': q})
    else:
        return HttpResponse('Please submit a search term.')
