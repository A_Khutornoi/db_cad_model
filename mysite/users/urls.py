from django.conf.urls import patterns, include, url

from users import views


urlpatterns = patterns('',
    
    url(r'^register/$', views.RegisterFormView.as_view()),
    url(r'^login/$', views.LoginFormView.as_view()),
    url(r'^logout/$', views.LogoutView.as_view()),
    url(r'^base_login/$', views.login_view),
    
    
)