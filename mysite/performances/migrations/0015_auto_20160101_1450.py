# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('performances', '0014_auto_20151224_0559'),
    ]

    operations = [
        migrations.CreateModel(
            name='Performance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.CharField(max_length=55)),
                ('img', models.ImageField(upload_to='', blank=True, null=True)),
                ('description', models.TextField()),
                ('time', models.DateTimeField()),
                ('pub_date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['time'],
            },
        ),
        migrations.DeleteModel(
            name='performances',
        ),
    ]
