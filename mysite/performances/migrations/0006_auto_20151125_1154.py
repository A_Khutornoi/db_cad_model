# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('performances', '0005_auto_20151125_1152'),
    ]

    operations = [
        migrations.AlterField(
            model_name='performances',
            name='img',
            field=models.ImageField(null=True, upload_to=None),
        ),
    ]
