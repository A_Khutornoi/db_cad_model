# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('performances', '0010_auto_20151130_2247'),
    ]

    operations = [
        migrations.AddField(
            model_name='performances',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 30, 20, 37, 29, 11504, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='performances',
            name='time',
            field=models.DateTimeField(unique_for_date='time'),
        ),
    ]
