# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='performances',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('img', models.ImageField(upload_to='')),
                ('description', models.CharField(max_length=255)),
                ('time', models.DateTimeField()),
            ],
        ),
    ]
