# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('performances', '0002_performances_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='performances',
            name='img',
            field=models.ImageField(upload_to=b'', blank=True),
        ),
    ]
