# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('performances', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='performances',
            name='name',
            field=models.CharField(default='name', max_length=55),
            preserve_default=False,
        ),
    ]
