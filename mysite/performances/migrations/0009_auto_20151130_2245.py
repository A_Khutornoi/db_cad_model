# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('performances', '0008_auto_20151130_2244'),
    ]

    operations = [
        migrations.AlterField(
            model_name='performances',
            name='img',
            field=models.ImageField(upload_to=None, null=True, blank=True),
        ),
    ]
