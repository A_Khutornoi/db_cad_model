from django.shortcuts import render,get_object_or_404
import datetime
from .models import Performance
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def index(request):
	performances_list = Performance.objects.all()

	now = datetime.datetime.now()
	perf = Performance.objects.filter(time__gt = now)
	
	paginator = Paginator(performances_list, 5)
	page = request.GET.get('page')
	try:
		contacts = paginator.page(page)
	except PageNotAnInteger:
	# If page is not an integer, deliver first page.
		contacts = paginator.page(1)
	except EmptyPage:
	# If page is out of range (e.g. 9999), deliver last page of results.
		contacts = paginator.page(paginator.num_pages)
	context = {'performances': contacts,'now':now, 'perf':perf[:5]}
	return render(request, 'performances.html', context)


def about(request, p):
	performance = get_object_or_404(Performance, pk = p)

	now = datetime.datetime.now()
	perf = Performance.objects.filter(time__gt = now)
	
	context = {'performance':performance,'now':now, 'perf':perf[:5]}
	return render(request, 'about_performance.html', context)

