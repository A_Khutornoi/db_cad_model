# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organizers', '0002_auto_20151111_0705'),
    ]

    operations = [
        migrations.CreateModel(
            name='Organizer',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('fio', models.CharField(max_length=70)),
                ('description', models.TextField()),
                ('comunications', models.TextField()),
                ('photo', models.ImageField(upload_to='')),
            ],
        ),
        migrations.DeleteModel(
            name='organizers',
        ),
    ]
