from django.contrib import admin
from .models import Organizer

class OrganizersAdmin(admin.ModelAdmin):
	class Media:
		css = {'all': ('my_own_admin.css',)}
	search_fields = ['fio']


admin.site.register(Organizer,OrganizersAdmin)
