from django.shortcuts import render,get_object_or_404
import datetime
from performances.models import Performance
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger



from .models import Organizer


def index(request):
	news_list = Organizer.objects.all()

	now = datetime.datetime.now()
	perf = Performance.objects.filter(time__gt = now)

	paginator = Paginator(news_list, 6)
	page = request.GET.get('page')
	try:
		contacts = paginator.page(page)
	except PageNotAnInteger:
	# If page is not an integer, deliver first page.
		contacts = paginator.page(1)
	except EmptyPage:
	# If page is out of range (e.g. 9999), deliver last page of results.
		contacts = paginator.page(paginator.num_pages)

	context = {'organizers': contacts,'now':now, 'perf':perf[:5]}
	return render(request, 'organizers.html', context)
# Create your views here.

def about(request,pk):
	organizer = get_object_or_404(Organizer, pk = pk)

	now = datetime.datetime.now()
	perf = Performance.objects.filter(time__gt = now)
	
	context = {'organizer':organizer,'now':now, 'perf':perf[:5]}
	return render(request, 'about_organizer.html', context)
