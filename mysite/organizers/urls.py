from django.conf.urls import patterns, url
from organizers import views
urlpatterns = patterns('',
url(r'^$', views.index, name='organizers'),
url(r'^(?P<pk>\d+)$', views.about, name='about'),
)